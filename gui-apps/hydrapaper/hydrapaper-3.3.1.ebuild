# Copyright 2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

PYTHON_COMPAT=( python3_{9..11} )

inherit meson python-single-r1 gnome2-utils xdg

SRC_URI="https://gitlab.gnome.org/GabMus/HydraPaper/-/archive/${PV}/HydraPaper-${PV}.tar.bz2"
S="${WORKDIR}/HydraPaper-${PV}"
KEYWORDS="~amd64"

DESCRIPTION="A Gtk utility to set different backgrounds for each monitor on GNOME "
HOMEPAGE="https://hydrapaper.gabmus.org/"

LICENSE="GPL-3"
SLOT="0"

IUSE="test"
#REQUIRED_USE="${PYTHON_REQUIRED_USE}"

DEPEND="
	dev-python/pillow
	gui-libs/libadwaita
	gui-libs/gtk:4
	dev-util/blueprint-compiler
"
BDEPEND="${DEPEND}"
RDEPEND="${PYTHON_DEPS}"


src_configure() {
	meson_src_configure
}

src_install() {
	meson_src_install	
	python_optimize
	python_fix_shebang "${ED}/usr/bin"
}

pkg_postinst() {
	xdg_pkg_postinst
	gnome2_schemas_update
}

pkg_postrm() {
	xdg_pkg_postrm
	gnome2_schemas_update
}
