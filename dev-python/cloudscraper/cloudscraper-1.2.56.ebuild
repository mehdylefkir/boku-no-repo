# Copyright 1999-2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

PYTHON_COMPAT=( python3_{9..11} )
inherit distutils-r1

DESCRIPTION="A Python module to bypass Cloudflare's anti-bot page"
HOMEPAGE="https://github.com/venomous/cloudscraper"
SRC_URI="https://github.com/VeNoMouS/cloudscraper/archive/refs/tags/${PV}.tar.gz"

RESTRICT="test"
KEYWORDS="~amd64"
LICENSE="MIT"
SLOT="0"

DEPEND="
	test? (
		dev-python/js2py[${PYTHON_USEDEP}]
		dev-python/responses[${PYTHON_USEDEP}]
	)"
RDEPEND="
	>=dev-python/requests-2.9.2[${PYTHON_USEDEP}]
	>=dev-python/requests-toolbelt-0.9.1[${PYTHON_USEDEP}]
"

distutils_enable_tests pytest
