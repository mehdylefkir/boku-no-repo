# Boku no repo

A Gentoo overlay with some packages (yet) unavailable in the main repository.


| Package  		 | Version | Links                                   		  |
|------------------------|---------|------------------------------------------------------|
| SAOImageDS9		 | 8.2.1   | https://github.com/SAOImageDS9/SAOImageDS9		  |
| Lollypop	  	 | 1.4.35  | https://gitlab.gnome.org/World/lollypop 		  |
| Komikku		 | 0.41.0  | https://gitlab.com/valos/Komikku          		  |
| Parsec   		 | 150.68  | https://parsec.app/                     		  |
| Looking Glass 	 | B3	   | https://looking-glass.io/		     		  |
| HydraPaper		 | 3.3.1   | https://hydrapaper.gabmus.org/			  |
