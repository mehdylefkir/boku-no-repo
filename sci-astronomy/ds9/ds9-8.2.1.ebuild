# Copyright 1999-2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit eutils autotools desktop

DESCRIPTION="SAOImageDS9 is an image display and visualization tool for astronomical data"
HOMEPAGE="https://sites.google.com/cfa.harvard.edu/saoimageds9/home"
SRC_URI="https://ds9.si.edu/download/source/${PN}.${PV}.tar.gz -> ${P}.tar.gz"

KEYWORDS="amd64"
LICENSE="GPL-3"
SLOT="0"

REQUIRED_USE=""

DEPEND="x11-libs/libX11
	sys-libs/zlib
	dev-libs/libxml2
	dev-libs/libxslt
	x11-libs/libXft
	dev-lang/tcl
	dev-lang/tk

"
RDEPEND=""
DOCS=( Readme Programs NEWS )

S="${WORKDIR}/SAOImageDS9"
ECONF_SOURCE="${S}/unix"


src_install() {
	dobin bin/ds9
	doicon ds9.png
	make_desktop_entry ds9 "SAOImage DS9" ds9
}

